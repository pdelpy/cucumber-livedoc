const fs = require('fs');
const parser = require('gherkin-parse');
const path = require('path');

const getAllFiles = function (dirPath, arrayOfFiles) {
    const files = fs.readdirSync(dirPath);
    arrayOfFiles = arrayOfFiles || [];
    files.forEach(function (file) {
        if (fs.statSync(dirPath + '/' + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + '/' + file, arrayOfFiles);
        } else {
            arrayOfFiles.push(path.join(__dirname, dirPath, '/', file));
        }
    });
    return arrayOfFiles;
};

let template = (results, tags) => {
  return `<!DOCTYPE html>
  <html>
  <head>
    <link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@6.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
  </head>
  <body>
    <div id="app">
      <v-app>
        <v-main>
        <v-app-bar
              color="white"
            >
              <v-toolbar-title>
              Live Documentation</v-toolbar-title>
          </v-app-bar>
          <v-container fluid>
          <v-row>
            <v-col cols="4">
              <v-card>
                <v-list-item three-line>
                  <v-list-item-content>
                    <div class="text-overline mb-4">
                      Figures
                    </div>
                    <v-list-item-title class="text-h5 mb-1">
                      {{ filter.length }} features
                    </v-list-item-title>
                  </v-list-item-content>
                </v-list-item>
              </v-card>
            </v-col>
            <v-col cols="4">
              <v-card>
                <v-list-item three-line>
                  <v-list-item-content>
                    <div class="text-overline mb-4">
                    Figures
                    </div>
                    <v-list-item-title class="text-h5 mb-1">
                      {{ scenariosLength }} scenarios
                    </v-list-item-title>
                  </v-list-item-content>
                </v-list-item>
              </v-card>
            </v-col>
          </v-row>
        <v-row>
        <v-col cols="3"><v-select
        v-model="value"
        :items="tags"
        chips
        label="Tags"
        multiple
        outlined
        persistent-hint
      ></v-select></v-col>
      <v-spacer></v-spacer>
      <v-col cols="3" style="text-align: right;">
      <v-chip>{{ filter.length }} Features</v-chip>
      </v-col>  
        </v-row>
        <v-row>
          <v-col cols="6"  v-for="(item,i) in filter" :key="i">
            <v-expansion-panels accordion>
              <v-expansion-panel>
                <v-expansion-panel-header>
                  <v-container>
                    <v-row style="margin-bottom:15px">
                        <v-chip v-for="(tag, t) in item.feature.tags" :key="t">{{ tag.name }}</v-chip>
                        <v-spacer></v-spacer>
                        <v-chip color="grey" label outlined style="color:white; margin:5px">{{ item.feature.children.length }}</v-chip>
                    </v-row>
                    <v-row><b>Feature : </b><span style="padding-left:5px">{{ item.feature.name }}</span></v-row>
                  </v-container>
                </v-expansion-panel-header>
                <v-expansion-panel-content>
                  <!-- <row v-for="(it, idx) in item.feature.children" :key="idx">
                    {{ JSON.stringify(it) }}
                  </row> -->
                <v-row v-for="(it, idx) in item.feature.children" :key="idx">
                  <v-expansion-panels accordion style="margin-top:10px;margin-bottom:10px;">
                    <v-expansion-panel>
                      <v-expansion-panel-header>
                        <v-container>
                          <v-row style="margin-bottom:15px">
                              <v-chip v-for="(tg, tx) in it.tags" :key="tx">{{ tg.name }}</v-chip>
                              <v-spacer></v-spacer>
                              <v-chip color="grey" label outlined style="color:white; margin:5px">{{ it.steps.length }}</v-chip>
                          </v-row>
                          <v-row><b>Scenario : </b><span style="padding-left:5px">{{ it.name }}</span></v-row>
                        </v-container>
                      </v-expansion-panel-header>
                      <v-expansion-panel-content>
                        <v-container>
                          <v-row v-for="(s,sx) in it.steps" :key="sx">
                            <v-col cols="12" style="padding-top:0px;padding-bottom:0px;">
                                <span style="margin-right:5px; color:rgb(101, 84, 192)">{{s.keyword}}</span>
                                {{s.text}}
                            </v-col>
                          </v-row> 
                        </v-container>
                      </v-expansion-panel-content>
                    </v-expansion-panel>
                  </v-expansion-panels>
                </v-row>
          </v-col>
        </v-row>
        </v-container>
        </v-main>
      </v-app>
    </div>
  
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <script>
      new Vue({
        el: "#app",
        vuetify: new Vuetify(),
        computed: {
          // get only
          filter: function () {
              const a = this.value.length > 0 ? 
                this.results.filter(i => {
                  return i.feature.tags.length > 0 ?
                    this.value.includes(i.feature.tags[0].name)
                    : false;
                })
                : this.results
                return a;
          },
          scenariosLength: function() {
            let res = 0;
            this.filter.forEach(el => {
              res = res + el.feature.children.length;
            });
            return res;
          }
        },
        methods: {},
        mounted(){},
        data() {
          return {
            onlyFailed: false,
            value: [],
            tags: ${JSON.stringify(tags)},
            results: ${JSON.stringify(results)}
          }
        },
      })
    </script>
  </body>
  </html>`;
};



const generate = (featureDir, outputPath) => {
    const filesPath = getAllFiles(featureDir);

    const gerkinsFiles = filesPath.map((file) => {
        return parser.convertFeatureFileToJSON(file);
    });
  
    let tags = [];
    
    gerkinsFiles.forEach((i) => {
        if (i.feature.tags.length > 0) {
            tags.push(i.feature.tags[0].name);
        }
    });

    tags = [...new Set(tags)];
    
    const escapeText = JSON.stringify(gerkinsFiles).replace(/'/g,"&quot;");
    const escapeTextJSON = JSON.parse(escapeText);
    
    const dir='dist'
      
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir, {
        recursive: true
      });
    }
    
    fs.writeFileSync(`${outputPath}/index.html`, template(escapeTextJSON, tags));
}

exports.doc = {
  generate,
};
